from django import template

from madtest.models import Article

register = template.Library()


@register.inclusion_tag('article_list.html')
def show_artiicles_list(category):
    article_list = Article.objects.filter(status=category)
    return {'article_list': article_list}
