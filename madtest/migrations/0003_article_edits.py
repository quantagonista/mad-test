# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-14 06:11
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('madtest', '0002_auto_20170913_1416'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='edits',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='Edits'),
        ),
    ]
