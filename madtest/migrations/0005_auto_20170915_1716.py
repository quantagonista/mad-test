# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-15 11:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('madtest', '0004_auto_20170914_1305'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='status',
            field=models.CharField(choices=[('writed', 'writed'), ('corrected', 'corrected'), ('approved', 'cpproved')], default='writed', max_length=12),
        ),
    ]
