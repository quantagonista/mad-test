# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-17 04:04
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('madtest', '0005_auto_20170915_1716'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='text',
            field=models.TextField(max_length=1000, verbose_name='Text'),
        ),
    ]
