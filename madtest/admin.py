# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.auth.models import Permission
# Register your models here.
from django.contrib.admin import ModelAdmin
from madtest.models import Article

class ArticleAdmin(ModelAdmin):
    list_display = ('title',)

admin.site.register(Article,ArticleAdmin)
admin.site.register(Permission)
