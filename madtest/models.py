# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
from django.db import models


class Article(models.Model):
    ARTICLE_STATUS = (
        ('writed', 'writed'),
        ('corrected', 'corrected'),
        ('published', 'published'),
        ('rejected', 'rejected')
    )

    title = models.CharField(max_length=100, verbose_name="Title")
    text = models.TextField(verbose_name="Text", max_length=1000)
    edits = models.TextField(max_length=100, verbose_name='Edits', blank=True, null=True)
    status = models.CharField(max_length=12, choices=ARTICLE_STATUS, default='writed')
    pub_date = models.DateTimeField('date published', default=datetime.datetime.now())

    class Meta:
        ordering = ['-pub_date']

    def __unicode__(self):
        return self.title

    def is_published(self):
        return self.status == 'published'

    def is_writed(self):
        return self.status == 'writed'

    def is_rejected(self):
        return self.status == 'rejected'

    def is_corrected(self):
        return self.status == 'corrected'
