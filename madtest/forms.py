from django.forms import ModelForm

from madtest.models import Article


class ArticleForm(ModelForm):
    class Meta:
        model = Article
        exclude = ['edits', 'status', 'pub_date']


class ExtendedArticleForm(ModelForm):
    class Meta:
        model = Article
        exclude = ['pub_date', 'status']
