"""madtest URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth.views import login, logout

from madtest.views import HomeView, ArticleView, ArticleDeleteView, add_article, edit_article, reject_article, \
    accept_article, publish_article

urlpatterns = [
    url(r'^accounts/login/$', login, name='login'),
    url(r'^accounts/logout/$', logout, {'next_page': '/'}, name='logout'),

    url(r'^admin/', admin.site.urls),
    url(r'^$', (HomeView.as_view()), name="home"),
    url(r'^home/(?P<category>\w+)$', (HomeView.as_view()), name="extended-home"),
    url(r'^article/new/$', add_article, name="add_article"),
    url(r'^article/(?P<id>\d+)$', ArticleView.as_view(), name="article"),
    url(r'^article/(?P<id>\d+)/delete/$', ArticleDeleteView.as_view(), name="article_delete"),
    url(r'^article/(?P<id>\d+)/edit/$', edit_article, name="article_edit"),
    url(r'^article/(?P<id>\d+)/(?P<user>\d+)$', edit_article, name="extended_article_edit"),
    url(r'^article/(?P<id>\d+)/reject/$', reject_article, name="article_reject"),
    url(r'^article/(?P<id>\d+)/accept/$', accept_article, name="article_accept"),
    url(r'^article/(?P<id>\d+)/publish/$', publish_article, name="article_publish"),


]
