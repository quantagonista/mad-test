# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Create your views here.
import datetime

from django.contrib import messages
from django.shortcuts import redirect, render
from django.urls import reverse
from django.views.generic import TemplateView

from madtest.forms import ArticleForm, ExtendedArticleForm
from madtest.models import Article


class HomeView(TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        user = self.request.user
        if user.is_authenticated():
            if 'category' in kwargs:
                category = kwargs['category']
                articles = Article.objects.all().filter(status=category)
            else:
                if user.has_perm('auth.writer') or user.has_perm('auth.corrector'):
                    articles = Article.objects.filter(status='writed')#.filter(status='rejected')

                elif user.has_perm('auth.editor'):
                    articles = Article.objects.filter(status='corrected')
        else:
            articles = Article.objects.filter(status='published')
        context = {'articles': articles}
        return context


class ArticleView(TemplateView):
    template_name = 'article.html'

    def get_context_data(self, **kwargs):
        id = kwargs['id']
        article = Article.objects.get(pk=id)
        context = {'article': article}
        return context


class ArticleDeleteView(TemplateView):
    def dispatch(self, request, *args, **kwargs):
        try:
            article = self.get_article(**kwargs)
            title = article.title
            article.delete()
            messages.add_message(request, messages.SUCCESS, u'Article "%s" successful deleted' % title)
            return redirect(reverse('home'))
        except:
            return redirect(reverse("home"))

    def get_article(self, **kwargs):
        article_id = kwargs['id']
        return Article.objects.get(pk=article_id)


def add_article(request):
    if request.method == "POST":
        form = ArticleForm(request.POST)
        if form.is_valid():
            article = form.save()
            article.pub_date = datetime.datetime.now()
            article.save()
            return redirect(reverse('extended-home', kwargs={'category': 'writed'}))
    else:
        form = ArticleForm()
        return render(request, 'madtest/article_form.html', {'form': form})


def edit_article(request, **kwargs):
    if request.user.is_authenticated():
        template_name = 'madtest/extended_article_form.html'
        article = Article.objects.get(id=kwargs['id'])
        user = request.user
        if user.has_perm('auth.corrector'):
            category = 'writed'
        elif user.has_perm('auth.editor'):
            category = 'corrected'

        if request.method == "POST":

            form = ExtendedArticleForm(request.POST, instance=article)
            if form.is_valid():
                article = form.save()
                article.pub_date = datetime.datetime.now()
                article.save()
                return redirect(reverse('extended-home', kwargs={'category': category}))
        else:
            form = ExtendedArticleForm(instance=article)
            return render(request, template_name, {'form': form})
    else:
        template_name = 'madtest/article_form.html'
        article = Article.objects.get(id=kwargs['id'])
        if request.method == "POST":
            form = ArticleForm(request.POST, instance=article)
            if form.is_valid():
                article = form.save()
                article.pub_date = datetime.datetime.now()
                article.status = 'writed'
                article.save()
                return redirect(reverse('extended-home', kwargs={'category': 'writed'}))
        else:
            form = ArticleForm(instance=article)
            return render(request, template_name, {'form': form})


def accept_article(request, **kwargs):
    article = Article.objects.get(id=kwargs['id'])
    article.status = 'corrected'
    article.save()
    messages.add_message(request, messages.SUCCESS, u'Article "%s" successful corrected' % article.title)
    return redirect(reverse('extended-home', kwargs={'category': 'writed'}))


def reject_article(request, id):
    article = Article.objects.get(id=id)
    article.status = 'rejected'
    article.save()
    messages.add_message(request, messages.SUCCESS, u'Article "%s" successful rejected' % article.title)
    if request.user.has_perm('auth.editor'):
        return redirect(reverse('extended-home', kwargs={'category': 'corrected'}))
    else:
        return redirect(reverse('extended-home', kwargs={'category': 'writed'}))


def publish_article(request, id):
    article = Article.objects.get(id=id)
    article.status = 'published'
    article.save()
    messages.add_message(request, messages.SUCCESS, u'Article "%s" successful published' % article.title)
    return redirect(reverse('extended-home', kwargs={'category': 'corrected'}))
